
# Poc ApiGateway

L’API Gateway agit comme un point d’entrée unique pour tous les services d’une architecture de microservices


## Exemple

#### 

Retourne les jours fériés d’un pays ou d’une société de gestion. On peut mettre les deux mais il faut être cohérent. On peut mettre une année pour sélectionner l’année des jours férié. Par défaut, c’est ceux de l’année en cours.

Endpoint de l'api referentiel-socle. L'api gateway modifie la réponse original.

```http
  GET /socle/calendrier/calendrier-jour-ferie?annee=2020&codePays=71&codeSocieteGestion=4
```


| Parameter | Type     | Description                |
| :-------- | :------- | :------------------------- |
| `annee  ` | `string` | Année |
| `codePays  ` | `string` | Code du pays |
| `codeSocieteGestion  ` | `string` | Code société de gestion |




Réponse referentiel-socle

```javascript
{
    "jourferie": "2020-01-01",
    "libelle": "Jour de l'An",
    "idPays": 71
}
```

Réponse api gateway

```javascript
{
    "date": "2020-01-01",
    "description": "Jour de l'An"
}
```