package com.example.apigateway.service;

import com.example.apigateway.data.ApiGatewayJourFerie;
import com.example.apigateway.mapper.SocleMapper;
import org.openapi.example.model.JourFerie;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.HashMap;

@Service
public class ReferentielSocleService {

    public ApiGatewayJourFerie[] calendrierJourFerie(JourFerie[] jourFeries) {
        if (jourFeries == null && jourFeries.length < 1) {
            return null;
        }

        return Arrays.stream(jourFeries).map(jourFerie -> SocleMapper.INSTANCE.toApiGatewayJourFerie(jourFerie)).toArray(ApiGatewayJourFerie[]::new);
    }

    public HashMap<String, String> paramDelaiJoursEntreDeuxDates() {
        HashMap<String, String> param = new HashMap<>();

        param.put("codePays", "71");
        param.put("dateDebut", "2019-10-01");
        param.put("dateFin", "2019-10-05");
        param.put("ouvrableOuOuvre", "OUVRABLE");

        return param;
    }
}
