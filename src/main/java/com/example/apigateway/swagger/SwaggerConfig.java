package com.example.apigateway.swagger;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import io.swagger.v3.oas.models.*;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import io.swagger.v3.oas.models.media.*;
import io.swagger.v3.oas.models.responses.ApiResponse;
import io.swagger.v3.oas.models.responses.ApiResponses;
import org.springframework.cloud.gateway.route.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.*;
import java.lang.reflect.Modifier;
import java.util.*;
import java.util.stream.Collectors;

@Configuration
public class SwaggerConfig {

    @Bean
    public OpenAPI customOpenAPI(RouteLocator routeLocator) throws FileNotFoundException {
        ExternalSwagger externalSwagger = new ExternalSwagger();

        return new OpenAPI()
                .components(buildComponents(externalSwagger.buildExternalComponents()))
                .paths(buildPathsv2(externalSwagger.buildExternalPaths()))
                .info(new Info().title("Apigateway").version("1.0.0")
                        .license(new License().name("Apache 2.0").url("http://springdoc.org")));
    }

    private Paths buildPaths(Paths paths) {
        Paths modifiedPaths = (Paths) paths.clone();
        modifiedPaths.forEach((s, pathItem) -> {
            pathItem.readOperations().forEach(operation -> {
                if (operation.getResponses().get("200") != null) {
                    ApiResponse apiResponse = operation.getResponses().get("200");
                    apiResponse.getContent().forEach((s1, mediaType) -> {
                        try {
                            String ref = mediaType.getSchema().getItems().get$ref();
                            String[] splittedRef = ref.split("/");
                            String className = splittedRef[splittedRef.length - 1];

                            if (getClass("ApiGateway" + className + ".java", "com.example.apigateway.data") != null) {
                                mediaType.getSchema().getItems().set$ref("#/components/schemas/ApiGateway" + className);
                            }
                        } catch (Exception e) {

                        }
                    });
                }
            });
        });

        return modifiedPaths;
    }

    private Paths buildPathsv2(Paths paths) throws FileNotFoundException {
        Paths modifiedPaths = new Paths();
        JsonObject routeGson = new Gson().fromJson(new FileReader("C:\\Users\\billy\\IdeaProjects\\poc-apigateway\\src\\main\\resources\\opengatewayapi\\openapi.json"), JsonObject.class);

        routeGson.getAsJsonArray("endpoints").forEach(e -> {
            JsonObject endpoint = e.getAsJsonObject();
            String endpointName = endpoint.get("internEndpoint").getAsString();
            String apigatewayEndpointName = endpoint.get("apigatewayEndpoint").getAsString();

            PathItem pathItem = paths.get(endpointName);
            if (pathItem != null) {
                if (endpoint.get("modifiedResponses") != null) {
                    pathItem.readOperations().get(0).setResponses(new Gson().fromJson(endpoint.get("modifiedResponses").toString(), ApiResponses.class));
                }

                modifiedPaths.put(apigatewayEndpointName, pathItem);
            }
        });

        return modifiedPaths;
    }


    private Components buildComponents(Components components) {
        String[] packages = {
                "com.example.apigateway.data"
        };

        Arrays.stream(packages).forEach(p -> {
            findAllClassesUsingClassLoader(p).forEach(aClass -> {
                Schema classSchema = new Schema();
                classSchema.setType(aClass.getTypeName());

                Arrays.stream(aClass.getDeclaredFields()).forEach(field -> {
                    if (Modifier.isPrivate(field.getModifiers())) {
                        Schema fieldSchema = new Schema();
                        fieldSchema.setType("string");
                        classSchema.addProperty(field.getName(), fieldSchema);
                    }
                });

                components.addSchemas(aClass.getSimpleName(), classSchema);
            });
        });

        return components;
    }

    private Set<Class> findAllClassesUsingClassLoader(String packageName) {
        InputStream stream = ClassLoader.getSystemClassLoader()
                .getResourceAsStream(packageName.replaceAll("[.]", "/"));
        BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
        return reader.lines()
                .filter(line -> line.endsWith(".class"))
                .map(line -> getClass(line, packageName))
                .collect(Collectors.toSet());
    }

    private Class getClass(String className, String packageName) {
        try {
            return Class.forName(packageName + "."
                    + className.substring(0, className.lastIndexOf('.')));
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }
}
