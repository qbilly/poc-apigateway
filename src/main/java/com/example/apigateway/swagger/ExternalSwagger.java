package com.example.apigateway.swagger;

import io.swagger.parser.OpenAPIParser;
import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.Paths;
import io.swagger.v3.parser.core.models.SwaggerParseResult;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ExternalSwagger {

    private SwaggerParseResult referentielSocle;
    private SwaggerParseResult referentielPersonne;

    public ExternalSwagger() {
        referentielSocle = new OpenAPIParser().readLocation("https://p1.cbp-group.com/referentiel-socle/v2/api-docs", null, null);
        referentielPersonne = new OpenAPIParser().readLocation("https://referentiel-personne.r2.internal.qa.apps.cbp.solutions/v3/api-docs/referentiel-personne-api", null, null);
    }

    public Paths buildExternalPaths() {
        OpenAPI openAPI = referentielSocle.getOpenAPI();
        return openAPI.getPaths();
    }

    public Components buildExternalComponents() {
        OpenAPI openAPI = referentielSocle.getOpenAPI();
        return openAPI.getComponents();
    }
}
