package com.example.apigateway.mapper;

import com.example.apigateway.data.ApiGatewayJourFerie;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;
import org.openapi.example.model.JourFerie;


@Mapper(componentModel = "spring")
public interface SocleMapper {

    SocleMapper INSTANCE = Mappers.getMapper(SocleMapper.class);

    @Mapping(target = "date", source = "jourferie")
    @Mapping(target = "description", source = "libelle")
    ApiGatewayJourFerie toApiGatewayJourFerie(JourFerie jourFerie);
}
