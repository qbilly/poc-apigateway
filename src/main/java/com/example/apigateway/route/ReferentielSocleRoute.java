package com.example.apigateway.route;

import com.example.apigateway.data.ApiGatewayJourFerie;
import com.example.apigateway.service.ReferentielSocleService;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import io.swagger.v3.oas.models.PathItem;
import io.swagger.v3.oas.models.parameters.Parameter;
import io.swagger.v3.oas.models.responses.ApiResponses;
import org.openapi.example.model.JourFerie;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gateway.filter.factory.rewrite.ModifyResponseBodyGatewayFilterFactory;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;

@Component
public class ReferentielSocleRoute {

    @Autowired
    ReferentielSocleService referentielSocleService;

    public RouteLocator referentielSocleRouteLocator(RouteLocatorBuilder builder) {
        System.out.println("Building routes...");
        RouteLocator routeLocator = builder.routes()
                .route(p -> p
                        .path("/socle/**")
                        .filters(f -> f
                                .rewritePath("/socle/(?<segment>.*)", "/referentiel-socle/private/open/${segment}")
                                .modifyResponseBody(JourFerie[].class, ApiGatewayJourFerie[].class, MediaType.APPLICATION_JSON_VALUE, (exchange, s) -> {
                                    return Mono.just(referentielSocleService.calendrierJourFerie(s));
                                })
                        )
                        .uri("https://p1.cbp-group.com/"))
                .build();

        return routeLocator;
    }


    @Bean
    public RouteLocator referentielSocleRouteLocator2(RouteLocatorBuilder builder) throws FileNotFoundException {
        System.out.println("Building routes v2...");
        JsonObject routeGson = new Gson().fromJson(new FileReader("C:\\Users\\billy\\IdeaProjects\\poc-apigateway\\src\\main\\resources\\opengatewayapi\\openapi.json"), JsonObject.class);

        RouteLocatorBuilder.Builder routes = builder.routes();

        routeGson.getAsJsonArray("endpoints").forEach(e -> {
            JsonObject endpoint = e.getAsJsonObject();
            routes.route(predicateSpec -> {
                return predicateSpec
                        .path(endpoint.get("apigatewayEndpoint").getAsString() + "/**")
                        .filters(gatewayFilterSpec -> {
                            gatewayFilterSpec = gatewayFilterSpec.rewritePath(endpoint.get("apigatewayEndpoint").getAsString() + "(?<segment>.*)",
                                    endpoint.get("prexix").getAsString() + endpoint.get("internEndpoint").getAsString() + "${segment}");

                            if (endpoint.get("modifiedParam") != null) {
                                try {
                                    Method method = referentielSocleService.getClass().getMethod(endpoint.get("modifiedParam").getAsString());
                                    HashMap<String, String> params = (HashMap<String, String>) method.invoke(referentielSocleService);

                                    for (Map.Entry<String, String> entry : params.entrySet()) {
                                        gatewayFilterSpec = gatewayFilterSpec.addRequestParameter(entry.getKey(), entry.getValue());
                                    }

                                } catch (Exception ex) {
                                    ex.printStackTrace();
                                }
                            }

                            if (endpoint.get("modifiedRequest") != null) {
                                JsonObject modifiedBody = endpoint.get("modifiedRequest").getAsJsonObject();
                                boolean isArray = modifiedBody.get("array").getAsBoolean();
                                Class inClass = null;
                                try {
                                    inClass = Class.forName("org.openapi.example.model." + modifiedBody.get("in").getAsString());
                                } catch (ClassNotFoundException ex) {
                                    throw new RuntimeException(ex);
                                }

                                if(isArray) {
                                    inClass = inClass.arrayType();
                                }

                                Class finalInClass = inClass;
                                gatewayFilterSpec = gatewayFilterSpec.modifyRequestBody(inClass, Object.class, MediaType.APPLICATION_JSON_VALUE, (serverWebExchange, o) -> {
                                    try {
                                        Method method = referentielSocleService.getClass().getMethod(modifiedBody.get("mapper").getAsString(), finalInClass);
                                        if (isArray) {
                                            Object[] param = {o};
                                            return Mono.just(method.invoke(referentielSocleService, param));
                                        }
                                        return Mono.just(method.invoke(referentielSocleService, o));
                                    } catch (Exception ex) {
                                        ex.printStackTrace();
                                        return null;
                                    }
                                });
                            }

                            if (endpoint.get("modifiedResponses") != null) {
                                ApiResponses apiResponses = new Gson().fromJson(endpoint.get("modifiedResponses").toString(), ApiResponses.class);
                                String ref = apiResponses.get("200").getContent().get("application/json").getSchema().getItems().get$ref();
                                String[] splittedRef = ref.split("/");
                                String className = splittedRef[splittedRef.length - 1];

                                Class inClass = getClass(className.substring(10) + ".java", "org.openapi.example.model");

                                String type = apiResponses.get("200").getContent().get("application/json").getSchema().getType();

                                if(type.equalsIgnoreCase("array")) {
                                    inClass = inClass.arrayType();
                                }

                                Class finalInClass = inClass;
                                gatewayFilterSpec = gatewayFilterSpec.modifyResponseBody(inClass, Object.class, MediaType.APPLICATION_JSON_VALUE, (exchange, s) -> {
                                    if (exchange.getResponse().getStatusCode().is2xxSuccessful()) {
                                        try {
                                            Method method = referentielSocleService.getClass().getMethod(endpoint.get("mapper").getAsString(), finalInClass);
                                            if (type.equalsIgnoreCase("array")) {
                                                Object[] param = {s};
                                                return Mono.just(method.invoke(referentielSocleService, param));
                                            }
                                            return Mono.just(method.invoke(referentielSocleService, s));
                                        } catch (Exception ex) {
                                            ex.printStackTrace();
                                            return null;
                                        }
                                    }
                                    return Mono.just(exchange.getResponse().getStatusCode());
                                });
                            }
                            return gatewayFilterSpec;
                        }).uri("https://p1.cbp-group.com/");
            });
        });;

        return routes.build();
    }

    private Class getClass(String className, String packageName) {
        try {
            return Class.forName(packageName + "."
                    + className.substring(0, className.lastIndexOf('.')));
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }
}
