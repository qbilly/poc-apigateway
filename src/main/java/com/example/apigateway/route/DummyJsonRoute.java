package com.example.apigateway.route;

import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

@Component
public class DummyJsonRoute {

    @Bean
    public RouteLocator dummyJsonRouteLocator(RouteLocatorBuilder builder) {
        return builder.routes()
                .route(p -> p
                        .path("/dummy/**")
                        .filters(f -> f
                                .rewritePath("/dummy/(?<segment>.*)", "/${segment}")
                                .removeJsonAttributes(true, "id")
                        ).uri("https://dummyjson.com/"))
                .build();
    }
}
